# JSharp (J#) for VS Code

This Visual Studio Code extension supports JSharp.Net (J#) with some syntax highlighting.

## Features

- Syntax highlighting
    (e.g. `for`, `while`, `switch...case`, `if...else...else if`, `try...catch...finally`, `do...while`)
- Code snippets
    (e.g. `for`, `while`, `switch...case`, `if...else...else if`, `try...catch...finally`, `do...while`)

## Installation

### From repository

Download the .vsix file and choose 'Install from VSIX...' from the app.

## Changelog

See the [changelog](https://gitlab.com/sjsepan/sjsepan.vscode-jsharp/blob/HEAD/CHANGELOG.md) for details.

## Issues

-

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/15/2025

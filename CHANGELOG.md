# Changelog

## [0.0.5]

- canonical layout reorg

## [0.0.4]

- fix manual install instructions in readme

## [0.0.3]

- fix repo url in pkg

## [0.0.2]

- rename publisher in manifest
- add more values in manifest

## [0.0.1]

- Initial release with syntax highlighting & code snippets

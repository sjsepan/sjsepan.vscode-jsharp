#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

# publish

vsce publish -i ./vscode-jsharp-0.0.5.vsix
npx ovsx publish vscode-jsharp-0.0.5.vsix --debug --pat <PAT>